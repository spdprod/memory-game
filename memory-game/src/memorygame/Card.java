package memorygame;

public class Card {
	private int id;
	private int order;
        private int randomN;
	/**
	 * Αρχικοποίηση των μεταβλητών.
	 */
	public Card(int id) {
		this.id = id;
                
	}
        public Card(int id, int order) {
            this.id = id;
            this.order = order;
        }
        
        public void setRandomN(int randomN) {
            this.randomN = randomN;
        }
	
        public void setOrder(int order) {
            this.order = order;
            
        }
        
        public int getId() {
            return id;
        }
        
        public int getOrder() {
            return order;
            
        }
	

}
