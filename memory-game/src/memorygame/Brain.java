/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package memorygame;

import java.util.ArrayList;
import java.util.Iterator;

/**
 *
 * @author Chris
 */
public class Brain {
    private ArrayList<Memory> memory;
    private int copies;
    
    public Brain() {
        memory = new ArrayList<>();
        copies = GameSettings.copies;
    }
    public int[] getPlay() {
      
        int[] answer = new int[copies];
        for(int i = 0;i < copies;i ++) {
            answer[i] = -1;
        }  
       
        int max = -1;
        for(Memory m : memory) {
            max = m.getReady();
        }
       
        for(Memory m : memory) {
            if(m.getReady() == max)
                answer = m.getAnswer();
        }
        
        return answer;
    }
    public void removeCard(Card c) {
        Iterator<Memory> it = memory.iterator();
        while(it.hasNext()) {
            Memory m = it.next();
            if(m.getId() == c.getId()) {
                it.remove();
            }
        }
    }
    public void processCard(int pos,Card c) {
       //If memory of card
        Iterator<Memory> it = memory.iterator();
        while(it.hasNext()) {
            Memory m = it.next();
            if(m.getId() == c.getId()) {
                m.processCard(pos, c);
                return;
            }
        }
        
        Memory m = new Memory();
        m.processCard(pos, c);
        memory.add(m);
        
        
        
    }
    
    public boolean isCardStored(Card c) {
        Iterator<Memory> it = memory.iterator();
        
        while(it.hasNext()) {
            Memory m = it.next();
            if(m.getId() == c.getId())
                return true;
        }
        return false;
    }
    
    
}
