package memorygame;
/**
 * 
 * Η κλάση GUI χρησιμοποιείται για να εμφανίσει τα αποτελέσματα στην οθόνη του χρήστη.
 *
 */
public class GUI {
	
	/**
	 * Σχεδιασμός των καρτών στην οθόνη
	 * @param numberOfCards Αριθμός των καρτών
	 * @param grid Πλέγμα ή τυχαία τοποθέτηση
	 */
	public void drawCards(int numberOfCards, boolean grid) {
		
	}
	
	/**
	 * Αφαίρεση των καρτών
	 * @param pos Θέσεις των καρτών
	 */
	public void removeCards(int[] pos) {
		
	}
	
	/**
	 * Σχεδιασμός πλαισίου στο εξωτερικό της κάρτας όταν αυτή επιλέγεται
	 * @param pos Θέση της κάρτας
	 */
	public void highlightCard(int pos) {
		
	}

}
