/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package memorygame;

import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.ArrayList;
import javax.swing.*;

/**
 *
 * @author Chris
 */
public class TestForm extends JFrame{
    private JButton b[];
    private Game game;
    
    public void Start() {
        game.Start();
    }
    public TestForm(Game game) {
        this.game = game;
        
        JPanel p = new JPanel(new GridLayout(GameSettings.rows,GameSettings.columns));
        b = new JButton[GameSettings.numberOfCards];
        for(int i = 0;i < b.length;i++){
            b[i] = new JButton("");
            b[i].addActionListener(new ActionListener() {
                public void actionPerformed(ActionEvent evt) {
                    JButton k = (JButton)evt.getSource();
                    if(k.getBackground() == Color.WHITE)
                        k.setBackground(Color.YELLOW);
                    else
                        k.setBackground(Color.WHITE);
                }
            });
            b[i].setBackground(Color.WHITE);
            b[i].setIcon(cardback());
            p.add(b[i]);
        }
        p.setSize(80 * GameSettings.columns, 80 * GameSettings.rows);
        setLayout(new FlowLayout());
        add(p);
        
        JButton endTurn = new JButton("End turn.");
        endTurn.setSize(120,40);
        endTurn.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent evt) {
                ArrayList<Integer> play;
                play = new ArrayList<>();
                            
                for(int i = 0;i < b.length;i++) {
                    if(b[i].getBackground() == Color.YELLOW) {
                        play.add(i);
                    }
                }
                
                int[] _play = new int[play.size()];
                for(int i = 0;i < _play.length;i++)
                    _play[i] = play.get(i);
                    
                if(_play.length <= GameSettings.copies)
                    game.playCards(_play);
            }
        });
        add(endTurn);
        setSize(p.getSize().width + 350,p.getSize().height + 130);
        
       
    }
    public void removeCard(int pos) {
        b[pos].setBackground(Color.WHITE);
        b[pos].setVisible(false);
    }
    private ImageIcon cardback() {
        return new ImageIcon(getClass().getResource("/memorygame/Images/card-back.gif"));
    }
    
    
    public void setIconById(int pos,int id) {
        System.out.println("yo re file");
        
        
        b[pos].setIcon(new ImageIcon(getClass().getResource("/memorygame/Images/" + String.valueOf(id) + ".png")));
    }
    
    public void setIconToCardback(int pos) {
        b[pos].setBackground(Color.WHITE);
        b[pos].setIcon(cardback());
    }
                
                
    
}
