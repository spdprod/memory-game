package memorygame;
/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */


/**
 *
 * @author Chris
 */
public class Memory {
    private int copies;
    private boolean predefinedOrder;
    private int id;
    
    private Card[] cards;
    private int[] answer;
    private int rightPositionCards;
    
    public Memory() {
       
        copies = GameSettings.copies;
        predefinedOrder = GameSettings.predefinedOrder;
        
        cards = new Card[copies];
        answer = new int[copies];
        
        for(int i = 0;i < copies;i++) {
            answer[i] = -1;
        }
    }
    
    public void processCard(int pos, Card c) {
        id = c.getId();
        answer[c.getOrder() - 1] = pos;
                
    }
    
    public boolean isReady() {
        for(int i = 0;i < copies;i ++) {
            if(answer[i] == -1)
                return false;
        }
        
        return true;
    }
    
    public int getReady() {
        rightPositionCards =0;
        for(int i = 0;i < copies;i++) {
            if(answer[i] != -1)
                rightPositionCards++;
            }
        
        return rightPositionCards;
    }
    
    public int getId() {
        return id;
    }
    public int[] getAnswer() {
        return answer;
    }
}
