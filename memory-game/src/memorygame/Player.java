package memorygame;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;
import java.util.Random;

public class Player {
	
	public enum PlayerType {
		REAL, ΒΟΤ
	}
	
	private String name;
	private PlayerType type;
	private int steps;
	
	/**
	 * Αρχικοποίηση των μεταβλητών
	 * @param name Όνομα του παίκτη
	 * @param type Τύπος του παίκτη (Πραγματικός ή AI)
	 */
	public Player(String name, PlayerType type) {
		this.name = name;
                this.type = type;
                steps = 0;
	}
	
	/**
	 * Αυξάνει τα βήματα του παίκτη 
	 * @param step Ο αριθμός που θα αυξηθούν τα βήματα
	 */
	public void addStep(int step) {
		steps = steps + step;
	}
        
        public PlayerType getType() { return type; }
        
        public enum BotType {
		ELEPHANT, KANGAROO, GOLDFISH
	}
	
	
	private Brain brain;
        
	private BotType botType;
	private boolean orderMatters;
        private int copies;
        
	public Player(String name, PlayerType type, BotType botType) {
		this.name = name;
                this.type = type;
                steps = 0;
		this.botType = botType;
                this.orderMatters = GameSettings.predefinedOrder;
                this.copies = GameSettings.copies;
                this.brain = new Brain();
	}
	
	/**
	 * Τοποθετεί την κάρτα που δίνεται στη μνήτη του
	 * @param pos Position της κάρτας
	 * @param id ID της κάρτας
	 */
	public void tryToRememberCard(int pos, Card c) {
		if(botType == BotType.GOLDFISH)
                    return;
                
                if(botType == BotType.KANGAROO) {
                    Random r = new Random();
                    boolean remember = r.nextBoolean();
                    if(remember){
                           brain.processCard(pos, c);
                    }
                }
                
                if(botType == BotType.ELEPHANT) {
                   brain.processCard(pos, c);
                }
	}
	
       public int[] getPlay(Card[] cards) {
           int[] answer = brain.getPlay();
           
           int[] remainingPos = new int[cards.length];
           int k = 0;
           for(int i = 0;i < remainingPos.length;i++) {
               
               if(cards[i] != null && !existsInArray(answer, i)) {
                   remainingPos[k] = i;
                   k++;
               }
           }
           
           //Randomise -1 in position
           int n = 0;
           for(int i : answer) {
               if(i == -1)
                   n++;
           }
          
         
           if(n != 0) {
            int[] array = pickNRandom(remainingPos,n);

            k = 0;
            for(int i = 0;i < answer.length;i++) {
                if(answer[i] == -1) {
                    answer[i] = array[k];
                    
                    k++;
                }

            }
           }
           
           for(int g : answer)
               System.out.println(g);
           
           return answer;
           
       }
       
       public static int[] pickNRandom(int[] array, int n) {

            List<Integer> list = new ArrayList(array.length);
            for (int i : array)
                list.add(i);
            Collections.shuffle(list);

            int[] answer = new int[n];
            for (int i = 0; i < n; i++)
                answer[i] = list.get(i);
            Arrays.sort(answer);
            return answer;

        }
       
       private boolean existsInArray(int[] array, int value)
       {
           for(int i = 0;i < array.length;i++) {
               if(array[i] == i)
                   return true;
           }
           return false;
       }
}
