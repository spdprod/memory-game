
package memorygame;
/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */


/**
 *
 * @author Chris
 */
public class GameSettings {
    
    public static boolean playingAgain = true;
    public static boolean isDuel = false;
    public static boolean predefinedOrder = false;
    public static boolean changeCardPosition = false;
    public static boolean randomCardsLocation = false;
    public static boolean doubleGame = false;
     
    public static int copies = 2;
    public static int uniqueCards = 12;
    public static int columns = 6;
    public static int rows = 4;
    public static int numberOfCards = 24;
    public static TestForm curForm;
    
    public static void setGrid(int _copies, boolean _doubleGame) {
        copies = _copies;
        doubleGame = _doubleGame;
        
        if(doubleGame)
            uniqueCards = 24;
        else
            uniqueCards = 12;
        
        numberOfCards = copies * uniqueCards;
        
        if(copies == 2) {
            if(doubleGame) {
                columns = 8;
                rows = 6;
            }
            else {
                columns = 6;
                rows = 4;
            }
        }
        
        
        if(copies == 3) {
            columns = 6;
            rows = 6;
        }
        
        if(copies == 4) {
            columns = 8;
            rows = 6;
        }
    }
    public GameSettings() {
        
    }
   /** 
    public boolean getPlayingAgain() { return playingAgain; }
    public boolean getIsDuel() { return isDuel; }
    public boolean getPredefinedOrder() { return predefinedOrder; }
    public boolean getChangeCardPosition() { return changeCardPosition; }
    public boolean getRandomCardsLocation() { return randomCardsLocation; }
    public boolean getDoubleGame() { return doubleGame; }
    public int getCopies() { return copies; }
    public int getUniqueCards() {return uniqueCards; }
    
    
    public void setPlayingAgain(boolean b) {
        playingAgain = b;
    }
    
    public void setIsDuel(boolean b) {
        isDuel = b;
    }
    
    public void setPredefinedOrder(boolean b) {
        predefinedOrder = b;
    }
    
    public void setChangeCardPosition(boolean b) {
        changeCardPosition = b;
    }
    
    public void setRandomCardsLocation(boolean b) {
        randomCardsLocation = b;
    }
    
    public void setDoubleGame(boolean b) {
        doubleGame = b;
        if(b)
            uniqueCards = 24;
    }
    
    public void setCopies(int c) {
        copies = c;
    }
   
    
    */
    
}
