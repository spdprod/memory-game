package memorygame;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Random;

/**
 * Η κλάση Deck χρησιμοποιείται για την δημιουργία λίστας απο κάρτες για το κάθε παιχνίδι.
 * 
 *
 */
public class Deck {
	
	private int copies;
	private int uniqueCards;
	private int numberOfCards;
	
	private boolean changeCardPosition;
	private boolean predefinedOrder;
	
	private Card[] cards;
	
	/**
	 * Αρχικοποίηση των μεταβλητών.
	
	 */
	public Deck() {
		this.copies = GameSettings.copies;
                this.uniqueCards = GameSettings.uniqueCards;
                this.changeCardPosition = GameSettings.changeCardPosition;
                this.predefinedOrder = GameSettings.predefinedOrder;
                numberOfCards = copies * uniqueCards;
                createDeck();
                
	}
	
        private void createDeck() {
            
            cards = new Card[numberOfCards];
            int k = 0;
            Random r = new Random();
            for(int i = 0; i < uniqueCards;i ++) {
                for(int j = 0;j < copies;j++) {
                    cards[k] = new Card(i);
                    cards[k].setOrder(j + 1); 
                    cards[k].setRandomN(r.nextInt());
                    k++;
                }
            }
            
        }
	/**
	 * Τοποθετεί τις κάρτες στο Array σε τυχαία σειρά.
	 * 
	 */
	public void shuffle() {
		ArrayList<Card> _cards = new ArrayList<>();
                for(int i = 0;i < cards.length;i++) {
                    _cards.add(cards[i]);
                }
                Collections.shuffle(_cards);
                
                cards = _cards.toArray(cards);
	}
	public void removeCards(int[] pos) {
             for(int i = 0;i < pos.length;i++) {
                 cards[pos[i]] = null;
             }
        }
	/** Κάνει έλεγχο αν οι κάρτες που δίνει o χρήστης στις θέσεις pos είναι ίδιες και στη σωστή σειρά όταν αυτό
	 * χρειάζεται 
	 * @param pos Οι θέσεις των καρτών στο πλέγμα
	 * @return Επιστρέφει true όταν είναι ίδιες (και στη σωστή σειρά όταν αυτό χρειάζεται)
	 */
	public boolean checkCards(int[] pos) {
            
                int prevId = cards[pos[0]].getId();
                
                         
                for(int i = 1;i < pos.length;i++) {
                    if(prevId != cards[pos[i]].getId()) {
                        
                        if(GameSettings.changeCardPosition)
                            swapCards(pos[0],pos[1]);
                        return false;
                    }
                }
                
                if(predefinedOrder) {
                    int prevOrder = cards[pos[0]].getOrder();
                    for(int j = 1;j < pos.length;j++) {
                        if(prevOrder > cards[pos[j]].getOrder())
                            return false;
                    }
                }
                                
                return pos.length == copies;
                
	}
	
	/**
	 * Επιστρέφει τις κάρτες που έχουν απομείνει.
	 * @return
	 */
	public Card[] getCards() {
            
		return cards;
	}
	public Card getCard(int pos) {
            return cards[pos];
        }
	/**
	 * Επιστρέφει το id της κάρτας σε συγκεκριμμένη θέση
	 * @param pos Θέση κάρτας
	 * @return
	 */
	public int getIdOfCard(int pos){
		return cards[pos].getId();
               
	}
	
	/** 
	 * Αλλαγή θέσης μεταξύ 2 καρτών
	 * @param pos1 Θέση πρώτης κάρτας
	 * 
	 * @param pos2 Θέση δεύτερης κάρτας
	 */
	public void swapCards(int pos1, int pos2) {
		Card temp = cards[pos1];
                cards[pos1] = cards[pos2];
                cards[pos2] = temp;
	}
	
	
	
}
