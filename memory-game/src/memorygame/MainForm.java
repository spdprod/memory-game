/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package memorygame;

import javax.swing.JRadioButton;

/**
 *
 * @author Chris
 */
public class MainForm extends javax.swing.JFrame {

    /**
     * Creates new form MainForm
     */
    public MainForm() {
        initComponents();
        initRadioButtonGroups();
        
    }
    
  private void initRadioButtonGroups()
  {
        //Mode
        buttonGroupMode.add(jRadioClassic);
        buttonGroupMode.add(jRadioDuel);
        jRadioDuel.setSelected(true);
                
      
        //Player 1
        buttonGroupP1.add(jRadioP1Real);
        buttonGroupP1.add(jRadioP1Easy);
        buttonGroupP1.add(jRadioP1Medium);
        buttonGroupP1.add(jRadioP1Hard);
        jRadioP1Real.setSelected(true);
        
        //Player 2
        buttonGroupP2.add(jRadioP2Real);
        buttonGroupP2.add(jRadioP2Easy);
        buttonGroupP2.add(jRadioP2Medium);
        buttonGroupP2.add(jRadioP2Hard);
        jRadioP2Real.setSelected(true);
        
        //Player 3
        buttonGroupP3.add(jRadioP3Real);
        buttonGroupP3.add(jRadioP3Easy);
        buttonGroupP3.add(jRadioP3Medium);
        buttonGroupP3.add(jRadioP3Hard);
        jRadioP3Real.setSelected(true);
        
        //Player 4
        buttonGroupP4.add(jRadioP4Real);
        buttonGroupP4.add(jRadioP4Easy);
        buttonGroupP4.add(jRadioP4Medium);
        buttonGroupP4.add(jRadioP4Hard);
        jRadioP4Real.setSelected(true);
        
        //Copies
        buttonGroupCopies.add(jRadioCopies2);
        buttonGroupCopies.add(jRadioCopies3);
        buttonGroupCopies.add(jRadioCopies4);
        jRadioCopies2.setSelected(true);
  }
    /**
     * This method is called from within the constructor to initialize the form.
     * WARNING: Do NOT modify this code. The content of this method is always
     * regenerated by the Form Editor.
     */
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        buttonGroupP1 = new javax.swing.ButtonGroup();
        buttonGroupP2 = new javax.swing.ButtonGroup();
        buttonGroupP3 = new javax.swing.ButtonGroup();
        buttonGroupP4 = new javax.swing.ButtonGroup();
        buttonGroupMode = new javax.swing.ButtonGroup();
        buttonGroupCopies = new javax.swing.ButtonGroup();
        jRadioDuel = new javax.swing.JRadioButton();
        jRadioClassic = new javax.swing.JRadioButton();
        jPanel1 = new javax.swing.JPanel();
        jCheckP1 = new javax.swing.JCheckBox();
        jTextP1 = new javax.swing.JTextField();
        jRadioP1Real = new javax.swing.JRadioButton();
        jRadioP1Easy = new javax.swing.JRadioButton();
        jRadioP1Medium = new javax.swing.JRadioButton();
        jRadioP1Hard = new javax.swing.JRadioButton();
        jSeparator2 = new javax.swing.JSeparator();
        jSeparator4 = new javax.swing.JSeparator();
        jCheckP2 = new javax.swing.JCheckBox();
        jTextP2 = new javax.swing.JTextField();
        jRadioP2Real = new javax.swing.JRadioButton();
        jSeparator5 = new javax.swing.JSeparator();
        jRadioP2Easy = new javax.swing.JRadioButton();
        jRadioP2Medium = new javax.swing.JRadioButton();
        jRadioP2Hard = new javax.swing.JRadioButton();
        jCheckP3 = new javax.swing.JCheckBox();
        jTextP3 = new javax.swing.JTextField();
        jRadioP3Real = new javax.swing.JRadioButton();
        jSeparator6 = new javax.swing.JSeparator();
        jRadioP3Easy = new javax.swing.JRadioButton();
        jRadioP3Medium = new javax.swing.JRadioButton();
        jRadioP3Hard = new javax.swing.JRadioButton();
        jCheckP4 = new javax.swing.JCheckBox();
        jTextP4 = new javax.swing.JTextField();
        jRadioP4Real = new javax.swing.JRadioButton();
        jSeparator7 = new javax.swing.JSeparator();
        jRadioP4Easy = new javax.swing.JRadioButton();
        jRadioP4Medium = new javax.swing.JRadioButton();
        jRadioP4Hard = new javax.swing.JRadioButton();
        jSeparator3 = new javax.swing.JSeparator();
        jLabel1 = new javax.swing.JLabel();
        jRadioCopies2 = new javax.swing.JRadioButton();
        jRadioCopies3 = new javax.swing.JRadioButton();
        jRadioCopies4 = new javax.swing.JRadioButton();
        jSeparator8 = new javax.swing.JSeparator();
        jCheckDoubleGame = new javax.swing.JCheckBox();
        jCheckChangePosition = new javax.swing.JCheckBox();
        jCheckPredefinedOrder = new javax.swing.JCheckBox();
        jCheckPlayingAgain = new javax.swing.JCheckBox();
        jCheckRandomLocation = new javax.swing.JCheckBox();
        jButton1 = new javax.swing.JButton();
        jSeparator9 = new javax.swing.JSeparator();
        jSeparator1 = new javax.swing.JSeparator();

        setDefaultCloseOperation(javax.swing.WindowConstants.EXIT_ON_CLOSE);

        jRadioDuel.setSelected(true);
        jRadioDuel.setText("Μονομαχία");

        jRadioClassic.setText("Κλασικό παιχνίδι");

        jPanel1.setBorder(javax.swing.BorderFactory.createLineBorder(new java.awt.Color(0, 0, 0)));
        jPanel1.setForeground(new java.awt.Color(51, 51, 51));
        jPanel1.setEnabled(false);

        jCheckP1.setSelected(true);
        jCheckP1.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jCheckP1ActionPerformed(evt);
            }
        });

        jTextP1.setText("Παίκτης 1");

        jRadioP1Real.setText("Real");

        jRadioP1Easy.setText("Εύκολο");

        jRadioP1Medium.setText("Μεσαίο");

        jRadioP1Hard.setText("Δύσκολο");
        jRadioP1Hard.setToolTipText("");

        jSeparator2.setOrientation(javax.swing.SwingConstants.VERTICAL);

        jCheckP2.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jCheckP2ActionPerformed(evt);
            }
        });

        jTextP2.setText("Παίκτης 2");
        jTextP2.setEnabled(false);

        jRadioP2Real.setText("Real");
        jRadioP2Real.setEnabled(false);

        jSeparator5.setOrientation(javax.swing.SwingConstants.VERTICAL);

        jRadioP2Easy.setText("Εύκολο");
        jRadioP2Easy.setEnabled(false);

        jRadioP2Medium.setText("Μεσαίο");
        jRadioP2Medium.setEnabled(false);

        jRadioP2Hard.setText("Δύσκολο");
        jRadioP2Hard.setToolTipText("");
        jRadioP2Hard.setEnabled(false);

        jCheckP3.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jCheckP3ActionPerformed(evt);
            }
        });

        jTextP3.setText("Παίκτης 3");
        jTextP3.setEnabled(false);

        jRadioP3Real.setText("Real");
        jRadioP3Real.setEnabled(false);

        jSeparator6.setOrientation(javax.swing.SwingConstants.VERTICAL);

        jRadioP3Easy.setText("Εύκολο");
        jRadioP3Easy.setEnabled(false);

        jRadioP3Medium.setText("Μεσαίο");
        jRadioP3Medium.setEnabled(false);

        jRadioP3Hard.setText("Δύσκολο");
        jRadioP3Hard.setToolTipText("");
        jRadioP3Hard.setEnabled(false);

        jCheckP4.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jCheckP4ActionPerformed(evt);
            }
        });

        jTextP4.setText("Παίκτης 4");
        jTextP4.setEnabled(false);

        jRadioP4Real.setText("Real");
        jRadioP4Real.setEnabled(false);

        jSeparator7.setOrientation(javax.swing.SwingConstants.VERTICAL);

        jRadioP4Easy.setText("Εύκολο");
        jRadioP4Easy.setEnabled(false);

        jRadioP4Medium.setText("Μεσαίο");
        jRadioP4Medium.setEnabled(false);

        jRadioP4Hard.setText("Δύσκολο");
        jRadioP4Hard.setToolTipText("");
        jRadioP4Hard.setEnabled(false);

        jLabel1.setText("Αριθμός αντιγράφων :");

        jRadioCopies2.setText("2");
        jRadioCopies2.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jRadioCopies2ActionPerformed(evt);
            }
        });

        jRadioCopies3.setText("3");
        jRadioCopies3.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jRadioCopies3ActionPerformed(evt);
            }
        });

        jRadioCopies4.setText("4");
        jRadioCopies4.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jRadioCopies4ActionPerformed(evt);
            }
        });

        jCheckDoubleGame.setText("Διπλό παιχνίδι");

        jCheckChangePosition.setText("Αλλαγή θέσης των καρτών");

        jCheckPredefinedOrder.setText("Άνοιγμα των καρτών με προκαθορισμένη σειρά");

        jCheckPlayingAgain.setSelected(true);
        jCheckPlayingAgain.setText("Ο παίκτης ξαναπαίζει");

        jCheckRandomLocation.setText("Τυχαία τοποθέτηση καρτών");

        jButton1.setText("Έναρξη!");
        jButton1.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButton1ActionPerformed(evt);
            }
        });

        javax.swing.GroupLayout jPanel1Layout = new javax.swing.GroupLayout(jPanel1);
        jPanel1.setLayout(jPanel1Layout);
        jPanel1Layout.setHorizontalGroup(
            jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel1Layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(jPanel1Layout.createSequentialGroup()
                        .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(jSeparator8, javax.swing.GroupLayout.Alignment.TRAILING)
                            .addComponent(jSeparator3)
                            .addComponent(jSeparator4)
                            .addGroup(jPanel1Layout.createSequentialGroup()
                                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                    .addGroup(jPanel1Layout.createSequentialGroup()
                                        .addComponent(jCheckP4)
                                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                        .addComponent(jTextP4, javax.swing.GroupLayout.PREFERRED_SIZE, 172, javax.swing.GroupLayout.PREFERRED_SIZE)
                                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                                        .addComponent(jRadioP4Real)
                                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                        .addComponent(jSeparator7, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                        .addComponent(jRadioP4Easy)
                                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                        .addComponent(jRadioP4Medium)
                                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                        .addComponent(jRadioP4Hard))
                                    .addGroup(jPanel1Layout.createSequentialGroup()
                                        .addComponent(jCheckP3)
                                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                        .addComponent(jTextP3, javax.swing.GroupLayout.PREFERRED_SIZE, 172, javax.swing.GroupLayout.PREFERRED_SIZE)
                                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                                        .addComponent(jRadioP3Real)
                                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                        .addComponent(jSeparator6, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                        .addComponent(jRadioP3Easy)
                                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                        .addComponent(jRadioP3Medium)
                                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                        .addComponent(jRadioP3Hard))
                                    .addGroup(jPanel1Layout.createSequentialGroup()
                                        .addComponent(jCheckP2)
                                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                        .addComponent(jTextP2, javax.swing.GroupLayout.PREFERRED_SIZE, 172, javax.swing.GroupLayout.PREFERRED_SIZE)
                                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                                        .addComponent(jRadioP2Real)
                                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                        .addComponent(jSeparator5, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                        .addComponent(jRadioP2Easy)
                                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                        .addComponent(jRadioP2Medium)
                                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                        .addComponent(jRadioP2Hard))
                                    .addComponent(jLabel1)
                                    .addGroup(jPanel1Layout.createSequentialGroup()
                                        .addComponent(jRadioCopies2)
                                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                                        .addComponent(jRadioCopies3)
                                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                                        .addComponent(jRadioCopies4))
                                    .addGroup(jPanel1Layout.createSequentialGroup()
                                        .addComponent(jCheckP1)
                                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                        .addComponent(jTextP1, javax.swing.GroupLayout.PREFERRED_SIZE, 172, javax.swing.GroupLayout.PREFERRED_SIZE)
                                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                                        .addComponent(jRadioP1Real)
                                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                        .addComponent(jSeparator2, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                        .addComponent(jRadioP1Easy)
                                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                        .addComponent(jRadioP1Medium)
                                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                        .addComponent(jRadioP1Hard)))
                                .addGap(0, 10, Short.MAX_VALUE)))
                        .addContainerGap())
                    .addGroup(jPanel1Layout.createSequentialGroup()
                        .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(jCheckDoubleGame)
                            .addComponent(jCheckPredefinedOrder)
                            .addComponent(jCheckChangePosition))
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                        .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(jCheckPlayingAgain)
                            .addComponent(jCheckRandomLocation))
                        .addGap(31, 31, 31))
                    .addComponent(jSeparator9, javax.swing.GroupLayout.Alignment.TRAILING)
                    .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanel1Layout.createSequentialGroup()
                        .addGap(0, 0, Short.MAX_VALUE)
                        .addComponent(jButton1, javax.swing.GroupLayout.PREFERRED_SIZE, 178, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addContainerGap())))
        );
        jPanel1Layout.setVerticalGroup(
            jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel1Layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(jSeparator4, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(jSeparator2, javax.swing.GroupLayout.PREFERRED_SIZE, 23, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                        .addComponent(jCheckP1)
                        .addComponent(jTextP1, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addComponent(jRadioP1Real)
                        .addComponent(jRadioP1Easy)
                        .addComponent(jRadioP1Medium)
                        .addComponent(jRadioP1Hard)))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(jSeparator5, javax.swing.GroupLayout.PREFERRED_SIZE, 23, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                        .addComponent(jCheckP2)
                        .addComponent(jTextP2, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addComponent(jRadioP2Real)
                        .addComponent(jRadioP2Easy)
                        .addComponent(jRadioP2Medium)
                        .addComponent(jRadioP2Hard)))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(jSeparator6, javax.swing.GroupLayout.PREFERRED_SIZE, 23, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                        .addComponent(jCheckP3)
                        .addComponent(jTextP3, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addComponent(jRadioP3Real)
                        .addComponent(jRadioP3Easy)
                        .addComponent(jRadioP3Medium)
                        .addComponent(jRadioP3Hard)))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(jSeparator7, javax.swing.GroupLayout.PREFERRED_SIZE, 23, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                        .addComponent(jCheckP4)
                        .addComponent(jTextP4, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addComponent(jRadioP4Real)
                        .addComponent(jRadioP4Easy)
                        .addComponent(jRadioP4Medium)
                        .addComponent(jRadioP4Hard)))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addComponent(jSeparator3, javax.swing.GroupLayout.PREFERRED_SIZE, 5, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addComponent(jLabel1)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jRadioCopies2)
                    .addComponent(jRadioCopies3)
                    .addComponent(jRadioCopies4))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jSeparator8, javax.swing.GroupLayout.PREFERRED_SIZE, 13, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jCheckDoubleGame)
                    .addComponent(jCheckPlayingAgain))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jCheckChangePosition)
                    .addComponent(jCheckRandomLocation))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jCheckPredefinedOrder)
                .addGap(1, 1, 1)
                .addComponent(jSeparator9, javax.swing.GroupLayout.PREFERRED_SIZE, 13, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jButton1)
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );

        jSeparator1.setForeground(new java.awt.Color(51, 51, 51));

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(jSeparator1, javax.swing.GroupLayout.Alignment.TRAILING)
                    .addGroup(layout.createSequentialGroup()
                        .addContainerGap()
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(jRadioDuel)
                            .addComponent(jRadioClassic))
                        .addGap(0, 0, Short.MAX_VALUE))
                    .addComponent(jPanel1, javax.swing.GroupLayout.Alignment.TRAILING, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                .addContainerGap())
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(jRadioDuel)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jSeparator1, javax.swing.GroupLayout.PREFERRED_SIZE, 2, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jRadioClassic)
                .addGap(7, 7, 7)
                .addComponent(jPanel1, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addContainerGap())
        );

        jRadioDuel.getAccessibleContext().setAccessibleName("jRadioDuel");

        pack();
    }// </editor-fold>//GEN-END:initComponents

    private void jRadioCopies4ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jRadioCopies4ActionPerformed
        checkSettings();
    }//GEN-LAST:event_jRadioCopies4ActionPerformed

    private void jRadioCopies3ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jRadioCopies3ActionPerformed
        checkSettings();
    }//GEN-LAST:event_jRadioCopies3ActionPerformed

    private void jRadioCopies2ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jRadioCopies2ActionPerformed
        checkSettings();

    }//GEN-LAST:event_jRadioCopies2ActionPerformed

    private void jCheckP4ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jCheckP4ActionPerformed
        boolean b = jCheckP4.isSelected();
        jTextP4.setEnabled(b);
        jRadioP4Real.setEnabled(b);
        jRadioP4Easy.setEnabled(b);
        jRadioP4Medium.setEnabled(b);
        jRadioP4Hard.setEnabled(b);
    }//GEN-LAST:event_jCheckP4ActionPerformed

    private void jCheckP3ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jCheckP3ActionPerformed
        boolean b = jCheckP3.isSelected();
        jTextP3.setEnabled(b);
        jRadioP3Real.setEnabled(b);
        jRadioP3Easy.setEnabled(b);
        jRadioP3Medium.setEnabled(b);
        jRadioP3Hard.setEnabled(b);
    }//GEN-LAST:event_jCheckP3ActionPerformed

    private void jCheckP2ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jCheckP2ActionPerformed
        boolean b = jCheckP2.isSelected();
        jTextP2.setEnabled(b);
        jRadioP2Real.setEnabled(b);
        jRadioP2Easy.setEnabled(b);
        jRadioP2Medium.setEnabled(b);
        jRadioP2Hard.setEnabled(b);
    }//GEN-LAST:event_jCheckP2ActionPerformed

    private void jCheckP1ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jCheckP1ActionPerformed
        boolean b = jCheckP1.isSelected();
        jTextP1.setEnabled(b);
        jRadioP1Real.setEnabled(b);
        jRadioP1Easy.setEnabled(b);
        jRadioP1Medium.setEnabled(b);
        jRadioP1Hard.setEnabled(b);
    }//GEN-LAST:event_jCheckP1ActionPerformed

    private void jButton1ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButton1ActionPerformed
        
        GameSettings.isDuel = jRadioDuel.isSelected();
        if(GameSettings.isDuel == false) {
            int copies = 0;
            GameSettings.playingAgain = jCheckPlayingAgain.isSelected();
            GameSettings.changeCardPosition = jCheckChangePosition.isSelected();
            GameSettings.predefinedOrder = jCheckPredefinedOrder.isSelected();

            if(jRadioCopies2.isSelected())
                copies = 2;
            else if(jRadioCopies3.isSelected())
                copies = 3;
            else if(jRadioCopies4.isSelected())
                copies = 4;
            GameSettings.setGrid(copies, jCheckDoubleGame.isSelected());
            
            GameSettings.randomCardsLocation = jCheckRandomLocation.isSelected();
            
            Game g = new Game();
            
            if(jCheckP1.isSelected())
                g.addPlayer(getPlayer(jTextP1.getText(),jRadioP1Real,jRadioP1Easy,jRadioP1Medium,jRadioP1Hard));
            
            if(jCheckP2.isSelected())
                g.addPlayer(getPlayer(jTextP2.getText(),jRadioP2Real,jRadioP2Easy,jRadioP2Medium,jRadioP2Hard));
             
            if(jCheckP3.isSelected())
                g.addPlayer(getPlayer(jTextP3.getText(),jRadioP1Real,jRadioP3Easy,jRadioP3Medium,jRadioP3Hard));
              
            if(jCheckP4.isSelected())
                g.addPlayer(getPlayer(jTextP4.getText(),jRadioP4Real,jRadioP4Easy,jRadioP4Medium,jRadioP4Hard));
            
            
            TestForm tf = new TestForm(g);
            GameSettings.curForm = tf;
            tf.setVisible(true);
            tf.Start();
        }
        
    }//GEN-LAST:event_jButton1ActionPerformed
     private Player getPlayer(String name,JRadioButton real,JRadioButton easy, JRadioButton medium,JRadioButton hard) {
        
       
        if(real.isSelected())
            return new Player(name, Player.PlayerType.REAL);
        
        if(easy.isSelected())
            return new Player(name, Player.PlayerType.ΒΟΤ, Player.BotType.GOLDFISH);
        
        if(medium.isSelected())
            return new Player(name, Player.PlayerType.ΒΟΤ, Player.BotType.KANGAROO);
         
        
        return new Player(name, Player.PlayerType.ΒΟΤ, Player.BotType.ELEPHANT);
                    
                    
    }
    private void checkSettings() {
       boolean b = jRadioCopies2.isSelected();
       jCheckDoubleGame.setEnabled(b);
       jCheckChangePosition.setEnabled(b);
       if(!b){
           jCheckDoubleGame.setSelected(false);
           jCheckChangePosition.setSelected(false);
       }
    }
    /**
     * @param args the command line arguments
     */
    public static void main(String args[]) {
        /* Set the Nimbus look and feel */
        //<editor-fold defaultstate="collapsed" desc=" Look and feel setting code (optional) ">
        /* If Nimbus (introduced in Java SE 6) is not available, stay with the default look and feel.
         * For details see http://download.oracle.com/javase/tutorial/uiswing/lookandfeel/plaf.html 
         */
        try {
            for (javax.swing.UIManager.LookAndFeelInfo info : javax.swing.UIManager.getInstalledLookAndFeels()) {
                if ("Nimbus".equals(info.getName())) {
                    javax.swing.UIManager.setLookAndFeel(info.getClassName());
                    break;
                }
            }
        } catch (ClassNotFoundException ex) {
            java.util.logging.Logger.getLogger(MainForm.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (InstantiationException ex) {
            java.util.logging.Logger.getLogger(MainForm.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (IllegalAccessException ex) {
            java.util.logging.Logger.getLogger(MainForm.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (javax.swing.UnsupportedLookAndFeelException ex) {
            java.util.logging.Logger.getLogger(MainForm.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        }
        //</editor-fold>

        /* Create and display the form */
        
        java.awt.EventQueue.invokeLater(new Runnable() {
            public void run() {
                new MainForm().setVisible(true);
            }
        });
        
        
    }

    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.ButtonGroup buttonGroupCopies;
    private javax.swing.ButtonGroup buttonGroupMode;
    private javax.swing.ButtonGroup buttonGroupP1;
    private javax.swing.ButtonGroup buttonGroupP2;
    private javax.swing.ButtonGroup buttonGroupP3;
    private javax.swing.ButtonGroup buttonGroupP4;
    private javax.swing.JButton jButton1;
    private javax.swing.JCheckBox jCheckChangePosition;
    private javax.swing.JCheckBox jCheckDoubleGame;
    private javax.swing.JCheckBox jCheckP1;
    private javax.swing.JCheckBox jCheckP2;
    private javax.swing.JCheckBox jCheckP3;
    private javax.swing.JCheckBox jCheckP4;
    private javax.swing.JCheckBox jCheckPlayingAgain;
    private javax.swing.JCheckBox jCheckPredefinedOrder;
    private javax.swing.JCheckBox jCheckRandomLocation;
    private javax.swing.JLabel jLabel1;
    private javax.swing.JPanel jPanel1;
    private javax.swing.JRadioButton jRadioClassic;
    private javax.swing.JRadioButton jRadioCopies2;
    private javax.swing.JRadioButton jRadioCopies3;
    private javax.swing.JRadioButton jRadioCopies4;
    private javax.swing.JRadioButton jRadioDuel;
    private javax.swing.JRadioButton jRadioP1Easy;
    private javax.swing.JRadioButton jRadioP1Hard;
    private javax.swing.JRadioButton jRadioP1Medium;
    private javax.swing.JRadioButton jRadioP1Real;
    private javax.swing.JRadioButton jRadioP2Easy;
    private javax.swing.JRadioButton jRadioP2Hard;
    private javax.swing.JRadioButton jRadioP2Medium;
    private javax.swing.JRadioButton jRadioP2Real;
    private javax.swing.JRadioButton jRadioP3Easy;
    private javax.swing.JRadioButton jRadioP3Hard;
    private javax.swing.JRadioButton jRadioP3Medium;
    private javax.swing.JRadioButton jRadioP3Real;
    private javax.swing.JRadioButton jRadioP4Easy;
    private javax.swing.JRadioButton jRadioP4Hard;
    private javax.swing.JRadioButton jRadioP4Medium;
    private javax.swing.JRadioButton jRadioP4Real;
    private javax.swing.JSeparator jSeparator1;
    private javax.swing.JSeparator jSeparator2;
    private javax.swing.JSeparator jSeparator3;
    private javax.swing.JSeparator jSeparator4;
    private javax.swing.JSeparator jSeparator5;
    private javax.swing.JSeparator jSeparator6;
    private javax.swing.JSeparator jSeparator7;
    private javax.swing.JSeparator jSeparator8;
    private javax.swing.JSeparator jSeparator9;
    private javax.swing.JTextField jTextP1;
    private javax.swing.JTextField jTextP2;
    private javax.swing.JTextField jTextP3;
    private javax.swing.JTextField jTextP4;
    // End of variables declaration//GEN-END:variables
}
