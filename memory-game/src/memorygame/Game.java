package memorygame;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.ArrayList;
import javax.swing.Timer;

/**
 * Η κλάση Game αναλαμβάνει την διεξαγωγή του παιχνιδιού και δέχεται ως εισόδους τις ενέργεις του χρήστη και
 * έχει ως αποτελέσματα οτιδήποτε χρειάζεται να αποτυπωθεί γραφικά.
 * 
 *
 */
public class Game {

	private boolean playingAgain;
	
	private ArrayList<Player> players;
	private int currentPlayer;
	
	private Deck deck;
	
	/**
	 * Δημιουργία των καρτών και αρχικοποίηση των μεταβλητών
	 * @param copies Αντίγραφα (2 ή 3 ή 4)
	 * @param unique Αριθμός από unique κάρτες
	 * @param playingAgain O παίκτης ξαναπαίζει όταν κερδίσει
	 */
	public Game() {
                players = new ArrayList<>();
                currentPlayer = -1;
		deck = new Deck();
                deck.shuffle();
                
                
	}
	public void Start() {
            nextPlayer();
        }
	/**
	 * Προσθήκη παίκτη
	 * @param p 
	 */
	public void addPlayer(Player p) {
                
		players.add(p);
	}
	
	/** 
	 * Αναλαμβάνει να ελέγχει την σωστή σειρά των παικτών
	 */
	private void nextPlayer() {
                
		currentPlayer = (currentPlayer + 1) % players.size();
                
                Player p = players.get(currentPlayer);
               
                if(p.getType() == Player.PlayerType.ΒΟΤ) {
                                                         
                    playCards(p.getPlay(deck.getCards()));
                }
                else
                {
                    
                }
                
                
	}
	
	/**
	 * Καλείται όταν επιλέγονται κάρτες απο κάποιον παίκτη ή απο το AI
	 * @param pos Position απο τις κάρτες που επιλέχθηκαν
	 * 
	 */
	public void playCards(int[] pos) {
            try {
                
                for(int k : pos) {
                       System.out.println("pos :" + k + " id : " + deck.getIdOfCard(k));
                      for(Player p : players) {
                          p.tryToRememberCard(k,deck.getCard(k));
                      }
                       GameSettings.curForm.setIconById(k,deck.getIdOfCard(k));
                }
                System.out.println("lol");
                if(deck.checkCards(pos)) {
                    System.out.println("yeah correct!");
                    deck.removeCards(pos);
                    
                    removeCardsWithDelay(pos);
                }
                else {
                   setCardbacksWithDelay(pos);
                }
            }
            catch (Exception ex) {
                System.out.println(ex.toString());
            }
           
           
            
                
	}
        
        private void removeCardsWithDelay(int[] pos) {
             ActionListener taskPerformer = new ActionListener() {
             public void actionPerformed(ActionEvent evt) {
                for(int k : pos) {
                    System.out.println("pos :" + k);
                    GameSettings.curForm.removeCard(k);
                 }
                 nextPlayer();
                }
            };
            Timer t = new Timer(2000,taskPerformer);
            t.setRepeats(false);
            t.start();
        }
        
        private void setCardbacksWithDelay(int[] pos) {
             ActionListener taskPerformer = new ActionListener() {
             public void actionPerformed(ActionEvent evt) {
                for(int k : pos) {
                    System.out.println("pos :" + k);
                    GameSettings.curForm.setIconToCardback(k);
                 }
                 nextPlayer();
                }
            };
            Timer t = new Timer(2000,taskPerformer);
            t.setRepeats(false);
            t.start();
        }
}
