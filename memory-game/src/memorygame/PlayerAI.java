package memorygame;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.Iterator;
import java.util.List;
import java.util.Random;

public class PlayerAI extends Player {
	
	public enum BotType {
		ELEPHANT, KANGAROO, GOLDFISH
	}
	
	
	private Brain brain;
        
	private BotType botType;
	private boolean orderMatters;
        private int copies;
        
	public PlayerAI(String name, PlayerType type, BotType botType) {
		super(name, type);
		this.botType = botType;
                this.orderMatters = GameSettings.predefinedOrder;
                this.copies = GameSettings.copies;
                this.brain = new Brain();
	}
	
	/**
	 * Τοποθετεί την κάρτα που δίνεται στη μνήτη του
	 * @param pos Position της κάρτας
	 * @param id ID της κάρτας
	 */
	public void tryToRememberCard(int pos, Card c) {
		if(botType == BotType.GOLDFISH)
                    return;
                
                if(botType == BotType.KANGAROO) {
                    Random r = new Random();
                    boolean remember = r.nextBoolean();
                    if(remember){
                           brain.processCard(pos, c);
                    }
                }
                
                if(botType == BotType.ELEPHANT) {
                   brain.processCard(pos, c);
                }
	}
	
       public int[] getPlay(Card[] cards) {
           int[] answer = brain.getPlay();
           
           int[] remainingPos = new int[cards.length];
           int k = 0;
           for(int i = 0;i < remainingPos.length;i++) {
               if(cards[i] != null && !existsInArray(answer, i)) {
                   remainingPos[k] = i;
                   k++;
               }
           }
           
           //Randomise -1 in position
           int n = 0;
           for(int i : remainingPos) {
               if(i == -1)
                   n++;
           }
           
           int[] array = pickNRandom(remainingPos,n);
           
           k = 0;
           for(int i = 0;i < answer.length;i++) {
               if(answer[i] == -1) {
                   answer[i] = array[k];
                   k++;
               }
                 
           }
           
           return answer;
           
       }
       
       public static int[] pickNRandom(int[] array, int n) {

            List<Integer> list = new ArrayList(array.length);
            for (int i : array)
                list.add(i);
            Collections.shuffle(list);

            int[] answer = new int[n];
            for (int i = 0; i < n; i++)
                answer[i] = list.get(i);
            Arrays.sort(answer);
            return answer;

        }
       
       private boolean existsInArray(int[] array, int value)
       {
           for(int i = 0;i < array.length;i++) {
               if(array[i] == i)
                   return true;
           }
           return false;
       }
       
	/**
	 * Τοποθετεί την κάρτα που δίνεται στη μνήτη του
	 * @param pos Position της κάρτας
	 * @param id ID της κάρτας
	 * @param order Προκαθορισμένη σειρά κάρτας
	 *
	public void tryToRememberCard(int pos, int id, int order) {
            
            if(botType == BotType.GOLDFISH)
                    return;
                
                if(botType == BotType.KANGAROO) {
                    Random r = new Random();
                    boolean remember = r.nextBoolean();
                    if(remember){
                         rememberedCards.add(new int[] {pos, id, order});
                    }
                }
                
                if(botType == BotType.ELEPHANT) {
                    rememberedCards.add(new int[] {pos, id, order});
                }
	}
	*/
        
	/**
	 * Το ΑΙ σκέφτεται και επιστρέφει την κίνηση του.
	 * @param cards Οι κάρτες πουν έχουν απομείνει
	 * @return Επιστρέφει τις θέσεις των καρτών που θα ανοίξει
	 *
	public int[] getPlay(Card[] cards) {
		if(orderMatters)
                    return getOrderPlay(cards);
                
                int[] answer = new int[copies];
                
                Iterator<int[]> it = rememberedCards.iterator();
                while(it.hasNext()) {
                    int[] s = it.next();
                    
                    if(cards[s[0]] == null) {
                        it.remove();
                        continue;
                    }
                    
                    int k = 0;
                    
                    for(int[] a : rememberedCards)
                    {
                        if(a[1] == s[1])
                            k++;
                    }
                    
                    if(k == copies)
                        return answer;
                        
                }
                    
                
                
                
	}
        
        private int[] getOrderPlay(Card[] cards) {
            
        }
        
        private int[] randomisePlay() {
            
        }
        
        */

}
